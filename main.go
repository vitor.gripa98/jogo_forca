package main

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"os/exec"
	"runtime"
	"strings"
)

func getWordList() ([]string, error) {
	file, err := os.Open("words.txt")

	if err != nil {
		fmt.Println("Error opening file", err)
		return nil, err
	}

	defer file.Close()

	content, err := ioutil.ReadAll(file)

	if err != nil {
		fmt.Println("Error reading file", err)
		return nil, err
	}

	animals := strings.Split(string(content), "\n")

	return animals, nil
}

func chooseWord(words []string) string {
	randomIndex := rand.Intn(len(words))

	return words[randomIndex]
}

func clearTerminal() {
	var cmd *exec.Cmd

	if runtime.GOOS == "windows" {
		cmd = exec.Command("cmd", "/c", "cls")
	} else {
		cmd = exec.Command("clear")
	}

	cmd.Stdout = os.Stdout
	cmd.Run()
}

func showInformation(lives int, word []string, guessedLetters []string) {
	fmt.Printf("Vidas: %d\n", lives)
	fmt.Println(word)
	fmt.Println("Letras já palpitadas: " + strings.Join(guessedLetters, ", "))
	fmt.Println("Digite uma letra:")
}

func hasUnderscore(word []string) bool {
	for _, letter := range word {
		if letter == "_" {
			return true
		}
	}

	return false
}

func alreadyGuessed(guessedLetters []string, guess string) bool {
	for _, letter := range guessedLetters {
		if strings.EqualFold(letter, guess) {
			return true
		}
	}

	return false
}

func getWordUnderscored(selectedWord string) []string {
	var word []string

	for range selectedWord {
		word = append(word, "_")
	}

	return word
}

func getWordWithGuess(word []string, selectedWord string, guess string) ([]string, bool) {
	isInWord := false

	for idx, letter := range selectedWord {
		if strings.EqualFold(string(letter), guess) {
			word[idx] = guess
			isInWord = true
		}
	}

	return word, isInWord
}

func main() {
	wordList, err := getWordList()

	if err != nil {
		fmt.Println("Não foi possível listar as palavras para o jogo")
	}

	selectedWord := chooseWord(wordList)

	word := getWordUnderscored(selectedWord)

	lives := 3
	guess := ""
	win := false

	var guessedLetters []string

	for lives > 0 {
		clearTerminal()

		if !hasUnderscore(word) {
			win = true
			break
		}

		showInformation(lives, word, guessedLetters)

		fmt.Scanln(&guess)

		if len(guess) > 1 {
			if strings.EqualFold(guess, selectedWord) {
				win = true
				break
			} else {
				lives--
			}
		} else if guess == "" {
			fmt.Println("Palpite inválido")
			continue
		} else {
			if alreadyGuessed(guessedLetters, guess) {
				fmt.Printf("O palpite '%s' já foi utilizado\n", guess)
				continue
			}

			guessedLetters = append(guessedLetters, guess)

			isInWord := false

			word, isInWord = getWordWithGuess(word, selectedWord, guess)

			if !isInWord {
				lives--
				continue
			}
		}
	}
	if win {
		fmt.Printf("Parabens a palavra era %s e você acertou\n", selectedWord)
	} else {
		fmt.Printf("Que pena! A palavra era %s\n", selectedWord)
	}
}
